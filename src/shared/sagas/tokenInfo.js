import { call, put } from 'redux-saga/effects';

import { fetchTokenInfo } from '&/shared/api';
import {
  tokenInfoLoading,
  tokenInfoSuccess,
  tokenInfoError, 
} from '&/shared/actions/actions';

export default function* fetchTokenInfoSaga() {
  yield put(tokenInfoLoading());
  try {
    const response = yield call(fetchTokenInfo);
    const { data } = response;
    yield put(tokenInfoSuccess(data));
  } catch (error) {
    yield put(tokenInfoError());
  }
}
