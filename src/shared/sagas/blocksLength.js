import { call, put } from 'redux-saga/effects';

import { fetchBlocksLength } from '&/shared/api';
import {
  blocksLengthLoading,
  blocksLengthSuccess,
  blocksLengthError,
} from '&/shared/actions/actions';

export default function* fetchBlocksLengthSaga() {
  yield put(blocksLengthLoading());
  try {
    const response = yield call(fetchBlocksLength);
    const { data } = response;
    yield put(blocksLengthSuccess(data));
  } catch (error) {
    yield put(blocksLengthError());
  }
}
